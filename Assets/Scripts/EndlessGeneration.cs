﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class EndlessGeneration : MonoBehaviour {

//    public const float maxViewDst = 300;
//    public Transform viewer;

//    public static Vector2 viewerPosition;
//    int chunkSize;
//    int chunksVisibleInViewDst;

//    //const int mapChunkSize = 241;

//    //[Range(0, 6)] public int levelOfDetail;
//    //public float noiseScale;

//    //public int octaves;
//    //[Range(0, 1)] public float persistance;
//    //public float lacunarity;

//    //public int seed;
//    //public Vector2 offset;

//    //public float meshHeightMultiplier;
//    //public AnimationCurve meshHeightCurve;

//    //public bool autoUpdate;

//    //public Terra


//    // Use this for initialization
//    void Start () {
//	}

//    void UpdateVisibleChunks()
//    {

//    }

//    private void MainRoad(Vector3 cityCentre)
//    {
//        int gridMapX = (int)(habitableMapArea.x / buildingLotSize);
//        int gridMapZ = (int)(habitableMapArea.y / buildingLotSize);

//        // Change this into random direction later
//        //int length = (UnityEngine.Random.Range(gridMapZ / 2, gridMapZ)) * spacing;
//        int length = gridMapZ * buildingLotSize/*UnityEngine.Random.Range(gridMapZ/100 * spacing, gridMapZ * spacing)*/;
//        Debug.Log("Length: " + length);
//        //Debug.Log("Transform Position: " + transform.position);


//        Vector3 mainStartNode = (transform.position + cityCentre + new Vector3(0, 0.01f, 0));
//        Vector3 mainEndNode = (mainStartNode + transform.forward * length);

//        mainStartNode -= new Vector3(0, 0, length / 2);
//        mainEndNode -= new Vector3(0, 0, length / 2);
//        //Debug.Log("Main End node: " + mainEndNode);

//        // This instantiate the main road and use the transformation rotation of the parent city generator
//        GameObject clone = Instantiate(roads, mainStartNode, transform.rotation);
//        clone.transform.localScale = Vector3.Scale(clone.transform.localScale, new Vector3(1, 1, length / roadWidth));

//        //clone.GetComponent<Renderer>().material.SetColor(_EmissionColor, Color.red);

//        // Put this into road layer
//        clone.layer = 8;

//        // This makes sure that it translate the whole object by half the length you transform it
//        clone.transform.position = mainStartNode + new Vector3(0, 0, length / 2);


//        numberOfRoads = 1;

//        // Create roads that are children to this road
//        //CreateRoad(mainStartNode, mainEndNode, transform.rotation, clone);

//        // Add Road component to the main road
//        clone.AddComponent<Road>();
//        //roadsList.Add(clone);

//        int newNumberOfNodes = (int)((mainEndNode - mainStartNode).magnitude / (buildingLotSize * roadSpacing));
//        int numberOfSpace = newNumberOfNodes * roadSpacing;
//        Debug.Log("Number of Nodes: " + newNumberOfNodes);

//        int depth = 0;

//        int numBranches = (int)Math.Ceiling(newNumberOfNodes * 2 * actualDensity);
//        for (int mainBranch = 0; mainBranch < numBranches; mainBranch++)
//        {
//            int nodeNumber = UnityEngine.Random.Range(1, newNumberOfNodes);

//            CreateRoad(mainStartNode, mainEndNode, transform.rotation, clone, nodeNumber, newNumberOfNodes, depth);
//        }
//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // Fix this

//        SetBuildingSpace(numberOfSpace, mainStartNode, clone);

//        //float detectionRadius = spacing / 2;
//        //for (int spaceNumber = 0; spaceNumber < numberOfSpace; spaceNumber++)
//        //{
//        //    Collider[] objectDetected = Physics.OverlapSphere(mainStartNode + clone.transform.forward * spacing * spaceNumber
//        //                                                      + clone.transform.right * spacing, detectionRadius, 1 << 8 | 1 << 10);
//        //    if (objectDetected.Length == 0)
//        //    {
//        //        Vector3 pos = mainStartNode + clone.transform.forward * spacing * spaceNumber + clone.transform.right * spacing;
//        //        int buildingNumber = (int)(Mathf.PerlinNoise(pos.x / noiseScale + offset.x, pos.z / noiseScale + offset.y) * buildings.Count) % buildings.Count;

//        //        GameObject buildingInstance = Instantiate(buildings[buildingNumber].building, mainStartNode + clone.transform.forward * spacing * spaceNumber
//        //                                                      + clone.transform.right * spacing /*+ clone.transform.up * buildings[buildingNumber].size.y / 2*/, Quaternion.identity);
//        //        buildingInstance.transform.LookAt(mainStartNode + clone.transform.forward * spacing * spaceNumber);
//        //        buildingInstance.layer = buildingLayer;
//        //    }
//        //    Collider[] objectDetectedLeft = Physics.OverlapSphere(mainStartNode + clone.transform.forward * spacing * spaceNumber
//        //                                                      - clone.transform.right * spacing, detectionRadius, 1 << 8 | 1 << 10);
//        //    if (objectDetectedLeft.Length == 0)
//        //    {
//        //        Vector3 pos = mainStartNode + clone.transform.forward * spacing * spaceNumber + clone.transform.right * spacing;
//        //        int buildingNumber = (int)(Mathf.PerlinNoise(pos.x / noiseScale + offset.x, pos.z / noiseScale + offset.y) * buildings.Count) % buildings.Count;
//        //        GameObject buildingInstance = Instantiate(buildings[buildingNumber].building, mainStartNode + clone.transform.forward * spacing * spaceNumber
//        //                                                      - clone.transform.right * spacing/* + clone.transform.up * buildings[buildingNumber].size.y / 2*/, Quaternion.identity);
//        //        buildingInstance.layer = buildingLayer;
//        //        buildingInstance.transform.LookAt(mainStartNode + clone.transform.forward * spacing * spaceNumber);
//        //    }
//        //}
//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    }

//    //// Dr Mikes analytics
//    //public int[] roadBins = new int[20];
//    public int[] iterationNumber = new int[3];

//    private void CreateRoad(Vector3 startNode, Vector3 endNode, Quaternion rotation, GameObject previousRoad, int nodeNumber, int numberOfNodes, int depth)
//    {
//        depth++;
//        //roadBins[Mathf.Clamp(numberOfNodes, 0, 19)]++;

//        //Vector3 marker = previousRoad.transform.forward;
//        //marker = Quaternion.LookRotation(previousRoad.transform.forward, previousRoad.transform.up) * marker;

//        // Instantiate the road
//        m_marker.transform.position = startNode;
//        m_marker.transform.rotation = rotation;

//        bool left = false;
//        bool right = false;

//        // Randomly choose which direction to turn
//        int randomNumber = UnityEngine.Random.Range(0, 2) % 2;
//        if (randomNumber == 1)
//        {

//            m_marker.transform.Rotate(0, 90, 0);
//            left = true;
//        }
//        else
//        {
//            m_marker.transform.Rotate(0, -90, 0);
//            right = true;
//        }
//        int turn = Convert.ToInt32(left) | (Convert.ToInt32(right) << 1);

//        // Sets the layer to the road layer
//        // This is set to 8, need to change it so that it can be set by the player


//        // Subdivides the road into small lengths specified as the spacing of the building


//        // Node number is the node where the new road is going to spawn
//        // This needs to be set to random later and find a way to reset nodes to start if while loop goes over
//        //int nodeNumber = 1/* UnityEngine.Random.Range(1, numberOfNodes)*/;
//        //Debug.Log("Number of Units: " + numberOfUnits);



//        // Check if another road is on this position
//        Dictionary<int, int> reference = previousRoad.GetComponent<Road>().occupiedNode;
//        int numberOfIterations = 0;
//        while (reference.ContainsKey(nodeNumber) && nodeNumber < numberOfNodes && numberOfIterations < 3)
//        {
//            int roadDirection = 0;
//            reference.TryGetValue(nodeNumber, out roadDirection);
//            //int checkLeft = roadDirection & left;
//            //int checkRight = roadDirection & right;

//            // This section checks which direction is the road facing and if there is a road on that node already
//            int checkDirection = roadDirection & turn;
//            if (checkDirection != 0)
//                nodeNumber++;
//            else
//                break;


//            if (nodeNumber + 1 > numberOfNodes && numberOfIterations < 2)
//            {
//                nodeNumber = 1;
//                numberOfIterations++;
//            }



//            //if (numberOfIterations == 2)
//            //{
//            //    left = !left;
//            //    right = !right;
//            //    turn = Convert.ToInt32(left) | (Convert.ToInt32(right) << 1);
//            //}
//            if (numberOfIterations > 1)
//                return;
//        }
//        iterationNumber[numberOfIterations]++;
//        if (nodeNumber > numberOfNodes || numberOfIterations > 2)
//        {
//            nodeNumber = 15;
//            //Destroy(clone);
//            //destroyedRoad++;
//            return;
//        }
//        //if (!reference.ContainsKey(nodeNumber))
//        //{
//        //    int addDirection = left | right;
//        //    reference.Add(nodeNumber, addDirection);
//        //}
//        int addDirection = turn/* | previousValue*/;
//        if (reference.ContainsKey(nodeNumber))
//        {
//            reference[nodeNumber] = reference[nodeNumber] | addDirection;
//            //int previousValue;
//            //reference.TryGetValue(nodeNumber, out previousValue);
//            //reference.Remove(nodeNumber);
//        }
//        else
//            reference.Add(nodeNumber, addDirection);
//        // Translate the position of the new road along the length of the previous road at set intervals
//        m_marker.transform.position = startNode + (previousRoad.transform.forward * nodeNumber * roadSpacing * buildingLotSize);

//        if (m_marker.transform.position.x < transform.position.x || m_marker.transform.position.x > transform.position.x + habitableMapArea.x ||
//            m_marker.transform.position.z < transform.position.z || m_marker.transform.position.z > transform.position.z + habitableMapArea.y)
//        {
//            return;
//        }

//        // Create a reference to the start node of the new road
//        Vector3 newStartNode = m_marker.transform.position;

//        // Creates a random length between 8 units and 15 units in length
//        float intendedLength = UnityEngine.Random.Range(8, maximumRoadLength) * buildingLotSize;

//        //RaycastHit hit;
//        //Physics.SphereCast(clone.transform.position, roadWidth + 2 * spacing, clone.transform.forward, out hit, intendedLength, 8);


//        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // This is used to check for roads that are parallel to the current road which is road width and building spacing distance away from it
//        RaycastHit[] hitColliders = Physics.SphereCastAll(m_marker.transform.position, roadWidth + buildingLotSize, m_marker.transform.forward, intendedLength, 1 << roadLayer/* | 1 << buildingLayer*/);

//        float roadLength = intendedLength;
//        //List<GameObject> buildingsOnRoad = new List<GameObject>();
//        for (int i = 0; i < hitColliders.Length; i++)
//        {
//            //if (hitColliders[i].collider.gameObject != previousRoad)
//            //    if (roadLength > hitColliders[i].distance)
//            //        roadLength = hitColliders[i].distance;

//            if (m_marker.transform != hitColliders[i].collider.transform)
//            {
//                float dotProduct = Vector3.Dot(hitColliders[i].collider.transform.forward, m_marker.transform.forward);

//                // This is used to check if its detecting road on the opposite side of the previous road
//                bool oppositeRoad = false;

//                // This is to make sure that density is fully shown when maxed out
//                if (density > 0.9f)
//                    oppositeRoad = (previousRoad == hitColliders[i].collider.GetComponent<Road>().parentRoad);
//                else
//                    oppositeRoad = false;


//                if ((dotProduct > 0.99f || dotProduct < -0.99f) && !oppositeRoad)
//                /*hitColliders[i].collider.gameObject != previousRoad )*/
//                {
//                    //Destroy(clone);
//                    //return;
//                    if (roadLength > hitColliders[i].distance/* && hitColliders[i].collider.gameObject.layer == roadLayer*/)
//                        roadLength = hitColliders[i].distance/* + roadWidth + spacing*//*.collider.GetComponent<Renderer>().bounds.size.z*/;
//                    //else if (hitColliders[i].collider.gameObject.layer == buildingLayer)
//                    //    buildingsOnRoad.Add(hitColliders[i].collider.gameObject);
//                }
//            }
//        }
//        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        if (roadLength < 1)
//        {
//            return;
//        }
//        //foreach(GameObject structure in buildingsOnRoad)
//        //{
//        //    if (newStartNode)
//        //}



//        RaycastHit[] buildingCheck = Physics.SphereCastAll(newStartNode, roadWidth / 2/* + spacing*/, m_marker.transform.forward, roadLength, 1 << buildingLayer);
//        for (int i = 0; i < buildingCheck.Length; i++)
//        {
//            if (buildingCheck[i].collider.gameObject.layer == buildingLayer)
//                Destroy(buildingCheck[i].collider.gameObject);
//        }


//        GameObject clone = Instantiate(roads, m_marker.transform.position, m_marker.transform.rotation);
//        instantiatedRoad++;

//        clone.layer = 8;

//        clone.AddComponent<Road>();

//        clone.GetComponent<Road>().parentRoad = previousRoad;

//        clone.GetComponent<Road>().buildingLayer = buildingLayer;


//        // Scale the road in relation to the length specified
//        //clone.transform.localScale = Vector3.Scale(clone.transform.localScale, new Vector3(1, 1, intendedLength));
//        clone.transform.localScale += new Vector3(0, 0, roadLength - 1);

//        // Translate the road to half its length so that its end connects with the previous road
//        clone.transform.Translate(new Vector3(0, 0, roadLength / 2));

//        // Create a reference to the end of the road
//        Vector3 newEndNode = clone.transform.position + clone.transform.forward * roadLength / 2;
//        //Debug.Log("End Node: " + newEndNode);

//        //roadsList.Add(clone);



//        int newNumberOfNodes = (int)((newEndNode - newStartNode).magnitude / (buildingLotSize * roadSpacing));
//        int numBranches = (int)Math.Ceiling(newNumberOfNodes * 2 * actualDensity);
//        numberOfRoads++;
//        int numberOfSpace = newNumberOfNodes * roadSpacing;
//        for (int i = 0; i < numBranches; i++)
//        {
//            nodeNumber = UnityEngine.Random.Range(1, newNumberOfNodes);
//            if (numberOfRoads < maximumNoOfRoads && depth < maximumDepth)
//                CreateRoad(newStartNode, newEndNode, clone.transform.rotation, clone, nodeNumber, newNumberOfNodes, depth);
//        }


//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // Fix this
//        SetBuildingSpace(numberOfSpace, newStartNode, clone);

//        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        return;
//    }

//    private void SetBuildingSpace(int numberOfSpace, Vector3 startNode, GameObject roadInstance)
//    {
//        // Sets the increment of checks in relation to the building lot size
//        Vector3 forwardIncrement = roadInstance.transform.forward * buildingLotSize;
//        Vector3 rightIncrement = roadInstance.transform.right * buildingLotSize;

//        for (int spaceNumber = 0; spaceNumber < numberOfSpace; spaceNumber++)
//        {
//            // Sets the position to check of the building on the right side of the road
//            Vector3 pos = startNode + forwardIncrement * spaceNumber + rightIncrement;

//            // Check and instantiate building on the right
//            InstantiateBuilding(startNode, pos, forwardIncrement, spaceNumber);

//            // Checks the left side of the road
//            pos -= rightIncrement * 2;

//            // Check and instantiate building on the left
//            InstantiateBuilding(startNode, pos, forwardIncrement, spaceNumber);
//        }
//    }

//    private void InstantiateBuilding(Vector3 startNode, Vector3 pos, Vector3 forwardIncrement, int spaceNumber)
//    {
//        float detectionRadius = buildingLotSize / 2.01f;

//        // Sets the layers to check
//        LayerMask layersToCheck = 1 << roadLayer | 1 << buildingLayer;

//        // Checks if there are any objects on that position
//        Collider[] objectDetected = Physics.OverlapSphere(pos, detectionRadius, layersToCheck);

//        if (objectDetected.Length != 0)
//        {
//            return;
//        }

//        int totalBuilding = (commercialBuildings.Count + residentialBuildings.Count);

//        bool residentialArea = (int)(Mathf.PerlinNoise(pos.x / overallNoiseScale + overallOffset.x, pos.z / overallNoiseScale + overallOffset.y) * totalBuilding) < (7 * totalBuilding / 10);
//        List<Building> buildingSet = null;

//        if (residentialArea)
//            buildingSet = residentialBuildings;
//        else
//            buildingSet = commercialBuildings;
//        // If there's no road or building in that particular space, instantiate a building in reference to Perlin noise in that position
//        int buildingNumber = 0;
//        if (totallyRandom)
//            buildingNumber = UnityEngine.Random.Range(0, buildingSet.Count - 1) % (buildingSet.Count - 1);
//        else
//            buildingNumber = (int)(Mathf.PerlinNoise(pos.x / noiseScale + offset.x, pos.z / noiseScale + offset.y) * buildingSet.Count) % buildingSet.Count;
//        //int buildingNumber = UnityEngine.Random.Range(0, buildingSet.Count - 1) % (buildingSet.Count - 1);

//        bool useDefault = false;
//        Vector2 allocation = buildingSet[buildingNumber].spaceAllocation;

//        if (allocation.sqrMagnitude > 2)
//        {
//            // This is used to check every allocation space that put the shape of the building into consideration
//            for (int i = 0; i < allocation.x; i++)
//            {
//                for (int j = 0; j < allocation.y; j++)
//                {
//                    Collider[] spaceCheck = Physics.OverlapSphere(pos + new Vector3(i, 0, j) * buildingLotSize, detectionRadius, layersToCheck);
//                    if (spaceCheck.Length != 0)
//                    {
//                        useDefault = true;
//                        break;
//                    }
//                }
//            }

//        }

//        GameObject buildingPrefab = buildingSet[buildingNumber].buildingObject;
//        Vector3 directionToFace = new Vector3(0, 0, 0);

//        if (useDefault)
//        {
//            if (residentialArea)
//                // Change the building to make to the default prefab when space is not big enough
//                buildingPrefab = defaultResidential.buildingObject;
//            else
//                buildingPrefab = defaultCommercial.buildingObject;

//            // Specify the direction of the road in reference to the default prefab allocation
//            directionToFace = startNode + forwardIncrement * spaceNumber;
//        }
//        else
//        {
//            // Reposition to centre of the building instance
//            pos = pos + new Vector3(allocation.x - 1, 0, allocation.y - 1) * buildingLotSize / 2;

//            // Specify the direction of the road
//            directionToFace = startNode + forwardIncrement * spaceNumber + forwardIncrement * (allocation.x - 1) / 2;
//        }




//        GameObject buildingInstance = Instantiate(buildingPrefab, pos, Quaternion.identity);

//        // Rotate the building so that it's facing the road
//        buildingInstance.transform.LookAt(directionToFace);

//        // Check if that particular prefab requires any model correction
//        ModelCorrection correction = buildingInstance.GetComponent<ModelCorrection>();

//        // Make the correction if required
//        if (correction != null)
//        {
//            correction.correctTheRotation();
//        }

//        // Sets the building to the building layer
//        buildingInstance.layer = buildingLayer;

//    }



//    // Update is called once per frame
//    void Update () {
		
//	}
//}

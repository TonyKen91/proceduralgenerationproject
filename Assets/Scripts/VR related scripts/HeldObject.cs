﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class HeldObject : MonoBehaviour {

    [HideInInspector]
    public Controller parent;

    public bool dropOnRelease;

    public float disconnectDistance = 0f;

    public UnityEvent pickUp;

    public UnityEvent drop;

    [HideInInspector]
    public Rigidbody simulator;

    public bool canPickUp = true;


    void Start()
    {
        simulator = new GameObject().AddComponent<Rigidbody>();
        simulator.name = "simulator";
        simulator.transform.parent = transform.parent;
        simulator.useGravity = false;
    }

    private void Update()
    {
        if (parent!=null)
        {
            simulator.velocity = (parent.transform.position - simulator.position) * 50f;

            if (disconnectDistance > 0f)
            {
                if (!GetComponent<Collider>().bounds.Contains(parent.transform.position) &&
                    Vector3.Distance(GetComponent<Collider>().bounds.ClosestPoint(parent.transform.position), parent.transform.position) > disconnectDistance)
                {
                    Drop();
                }
            }
        }
    }

    public void PickUp ()
    {
        pickUp.Invoke();
        if (pickUp.GetPersistentEventCount() == 0)
        {
            DefaultPickUp();
        }
        // When you pick something up triggers a haptic pulse which creates a sense of touch by applying force, vibration or motion
        parent.controller.TriggerHapticPulse(3999);
    }

    public void Drop ()
    {
        drop.Invoke();
        // Just checks how many listeners you've added and if none, use default
        if (drop.GetPersistentEventCount() == 0)
        {
            DefaultDrop();
        }
        if (parent != null)
        {
            parent.controller.TriggerHapticPulse(3999);
            parent = null;
        }
    }

    public void DefaultPickUp ()
    {
        if (canPickUp)
        {
            transform.parent = parent.transform;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public void DefaultDrop()
    {
        transform.parent = null;
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().velocity = simulator.velocity;
        parent = null;
    }
}

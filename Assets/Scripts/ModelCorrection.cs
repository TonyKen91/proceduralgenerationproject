﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelCorrection : MonoBehaviour {

    [SerializeField] private Vector3 properRotation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void correctTheRotation()
    {
        transform.Rotate (properRotation);
    }
}

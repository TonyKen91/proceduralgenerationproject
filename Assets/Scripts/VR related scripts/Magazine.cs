﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magazine : MonoBehaviour {

    public int roundsLeft;

    [HideInInspector]
    public MagSlot slot;
    MagSlot possibleSlot;

    private void Update()
    {
        if(GetComponent<HeldObject>().parent != null && possibleSlot != null && slot == null)
        {
            transform.rotation = possibleSlot.transform.rotation;
            transform.position = ClosestPointOnLine(transform.position, possibleSlot.pointA.position, possibleSlot.pointB.position);
            if (transform.position == possibleSlot.pointA.position)
            {
                AttachMagSlot(possibleSlot);
                possibleSlot = null;
            }
            else if (transform.position == possibleSlot.pointB.position)
            {
                possibleSlot = null;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (slot == null && other.GetComponent<MagSlot>() != null)
        {
            possibleSlot = other.GetComponent<MagSlot>();
        }
    }

    private void AttachMagSlot(MagSlot other)
    {
        other.AttachMag(this);

        // Stop from flying away
        GetComponent<BoxCollider>().enabled = false;

        GetComponent<HeldObject>().Drop();

        Transform attachPoint = slot.transform.Find("AttachPoint");
        // rotate before attaching it
        transform.position = attachPoint.position;
        transform.rotation = attachPoint.rotation;
        transform.parent = attachPoint;
    }

    public void PickUp()
    {
        if (slot != null)
        {
            slot.DetachMag();
        }
        // This will always pick up the object
        GetComponent<HeldObject>().DefaultPickUp();
    }

    public void Drop()
    {
        if (slot == null)
        {
            GetComponent<BoxCollider>().enabled = true;
            GetComponent<HeldObject>().DefaultDrop();
        }
    }


    Vector3 ClosestPointOnLine(Vector3 point, Vector3 pointA, Vector3 pointB)
    {
        Vector3 va = pointA;
        Vector3 vb = pointB;

        Vector3 vVector1 = point - va;

        Vector3 vVector2 = (vb - va).normalized;

        float t = Vector3.Dot(vVector2, vVector1);

        if (t <= 0)
            return va;

        if (t >= Vector3.Distance(va, vb))
            return vb;

        // Dot point in local position
        Vector3 vVector3 = vVector2 * t;

        // Exact wall position closest point on the line
        Vector3 vClosestPoint = va + vVector3;

        return vClosestPoint;
    }
}

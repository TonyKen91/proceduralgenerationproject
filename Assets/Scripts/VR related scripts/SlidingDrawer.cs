﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HeldObject))]
public class SlidingDrawer : MonoBehaviour {

    Transform parent;
    public Transform pointA;
    public Transform pointB;

    Vector3 offset;

    HeldObject heldObject;

	// Use this for initialization
	void Start () {
        heldObject = GetComponent<HeldObject>();
	}
	
	// Update is called once per frame
	void Update () {
		if (parent != null)
        {
            // Closest point on the line of the controller subtract the offset so it doesn't just jump around
            transform.position = ClosestPointOnLine(parent.position) - offset;
        }
	}

    public void PickUp()
    {
        parent = heldObject.parent.transform;

        offset = parent.position - transform.position;
        Debug.Log("Pick up is called");
    }

    public void Drop()
    {
        // So there is no big jump in velocity when you drop it
        heldObject.simulator.transform.position = transform.position + offset;

        Debug.Log("Drop has been called");

        // Makes the parent the rigidbody simulator so it will just slide
        parent = heldObject.simulator.transform;
    }

    Vector3 ClosestPointOnLine (Vector3 point)
    {
        Vector3 va = pointA.position + offset;
        Vector3 vb = pointB.position + offset;

        Vector3 vVector1 = point - va;

        Vector3 vVector2 = (vb - va).normalized;

        float t = Vector3.Dot(vVector2, vVector1);

        if (t <= 0)
            return va;

        if (t >= Vector3.Distance(va, vb))
            return vb;

        // Dot point in local position
        Vector3 vVector3 = vVector2 * t;

        // Exact wall position closest point on the line
        Vector3 vClosestPoint = va + vVector3;

        return vClosestPoint;
    }
}

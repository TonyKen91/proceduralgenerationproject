﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HeldObject))]
public class Gun : MonoBehaviour {

    public GameObject projectile;
    public float power;
    public Transform firepoint;

    public Valve.VR.EVRButtonId shootButton;

    HeldObject heldObject;

    public MagSlot magSlot;

    public TouchPosition MagDropLocation;


    public bool automatic;
    public float cooldownTime;
    float time;

	// Use this for initialization
	void Start () {
        heldObject = GetComponent<HeldObject>();
	}
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<HeldObject>().parent != null && GetComponent<HeldObject>().parent.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad))
        {
            if (GetComponent<HeldObject>().parent.CurrentTouchPosition() == MagDropLocation)
            {
                magSlot.DetachMag();
            }
        }
        if (time > 0f)
        {
            time -= Time.deltaTime;
        }
        else
        {
            if (magSlot.currentMag != null)
            {
                Magazine mag = magSlot.currentMag;
                if (mag != null && mag.roundsLeft > 0 && heldObject.parent != null && ((heldObject.parent.controller.GetPressDown(shootButton) && !automatic) || (heldObject.parent.controller.GetPress(shootButton) && automatic)))
                {
                    time = cooldownTime;
                    GameObject proj = Instantiate(projectile, firepoint.position, firepoint.rotation);
                    proj.GetComponent<Rigidbody>().velocity = firepoint.forward * power;
                    mag.roundsLeft--;
                }
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagSlot : MonoBehaviour {

    public Magazine currentMag;

    public Transform pointA;
    public Transform pointB;

    public void AttachMag (Magazine mag)
    {
        if (currentMag == null)
        {
            currentMag = mag;
            currentMag.slot = this;
            currentMag.GetComponent<HeldObject>().canPickUp = false;
        }
    }

    public void DetachMag ()
    {
        if (currentMag != null)
        {
            currentMag.slot = null;
            currentMag.GetComponent<HeldObject>().canPickUp = true;
            currentMag.Drop();
            currentMag = null;
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

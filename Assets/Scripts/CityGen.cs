﻿
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;


//namespace CityGeneration
//{


//    [System.Serializable]
//    public class Building
//    {
//        //public string name;
//        public GameObject building;
//        public Vector3 size;

//        public Building(GameObject newBuilding, Vector3 newSize)
//        {
//            building = newBuilding;
//            //size = newSize;
//            //size = building.transform.localScale;
//            size = building.GetComponent<Renderer>().bounds.size;
//            // or to be more precise building.getComponentbound
//        }
//    }

//    public class CityGenerator : MonoBehaviour
//    {


//        [SerializeField] private List<Building> buildings = new List<Building>();
//        [SerializeField] private List<GameObject> houses;
//        [SerializeField] private GameObject roads;
//        [SerializeField] private Vector2 habitableMapArea = new Vector2(50, 50);
//        [SerializeField] private int spacing = 3;
//        [SerializeField] private float noiseScale = 70.0f;
//        [SerializeField] private Terrain landMap;
//        [SerializeField] private float offsetX = 0;
//        [SerializeField] private float offsetY = 0;
//        [SerializeField] private float terrainHeightLimit = 50;
//        [SerializeField] private float steepnessLimit = 20;
//        [SerializeField] private float roadHeightLimit = 50;
//        [SerializeField] private float roadsteepnessLimit = 20;
//        [SerializeField] private int roadGridSpacing = 3;


//        private int buildingMaxSize;
//        private int buildingMinSize;
//        private float roadWidth;
//        private int numberOfRoads;
//        private float density = 1f;
//        private int maximumRoadLength = 50;
//        private int maximumDepth = 100;
//        private int maximumNoOfRoads = 1;
//        private int instantiatedRoad = 0;
//        private int destroyedRoad = 0;

//        private GameObject m_marker;

//        //private List<GameObject> roadsList;
//        //private float roadWidth;

//        [SerializeField] private Vector3 cityCentre;

//        //[SerializeField] private GameObject testBuilding;

//        // Use this for initialization
//        void Start()
//        {
//            m_marker = new GameObject();

//            roadWidth = roads.GetComponent<Renderer>().bounds.size.x;
//            cityCentre = new Vector3(habitableMapArea.x / 2, 0, 0);
//            //CompleteGrid();
//            MainRoad();
//            Debug.Log("Number Of Roads: " + numberOfRoads);
//            Debug.Log("Instantiated roads: " + instantiatedRoad);
//            Debug.Log("Destroyed roads: " + destroyedRoad);
//        }


//        private void CompleteGrid()
//        {

//            int gridMapX = (int)(habitableMapArea.x / spacing);
//            int gridMapZ = (int)(habitableMapArea.y / spacing);
//            int[,] buildingNumberArray = new int[gridMapX, gridMapZ];


//            for (int x = 0; x < gridMapX; x++)
//            {
//                for (int z = 0; z < gridMapZ; z++)
//                {
//                    //int n = Random.Range(0, buildings.Count);
//                    buildingNumberArray[x, z] = (int)(Mathf.PerlinNoise(x / noiseScale + offsetX, z / noiseScale + offsetY) * buildings.Count) % buildings.Count;
//                    if ((x % roadGridSpacing == 0 || z % roadGridSpacing == 0) && buildingNumberArray[x, z] != 0)
//                    {
//                        buildingNumberArray[x, z] = -1;
//                    }

//                }
//            }

//            // This kind of solution makes sure that you don't have conversion issues
//            //for (int x = 0; x < habitableMapArea.x; x += spacing)
//            //    for (int z = 0; z < habitableMapArea.y; z += spacing)


//            // This might involve conversion issues
//            for (int x = 0; x < gridMapX; x++)
//            {
//                for (int z = 0; z < gridMapZ; z++)
//                {
//                    int buildingNumber = buildingNumberArray[x, z];
//                    float y = Terrain.activeTerrain.SampleHeight(transform.position + new Vector3(x * spacing, 0, z * spacing));
//                    Vector3 position;
//                    if (buildingNumber < 0)
//                    {
//                        position = transform.position + new Vector3(x * spacing, 0.01f + y, z * spacing);
//                    }
//                    else
//                    {
//                        position = transform.position + new Vector3(x * spacing, buildings[buildingNumber].size.y / 2.0f + y, z * spacing);
//                    }

//                    // This normalise the position to terrain coordinate and then use that information to check the steepness at that point
//                    float normalizedX = (position.x - Terrain.activeTerrain.GetPosition().x) / Terrain.activeTerrain.terrainData.size.x;
//                    float normalizedY = (position.z - Terrain.activeTerrain.GetPosition().z) / Terrain.activeTerrain.terrainData.size.z;
//                    float steepness = Terrain.activeTerrain.terrainData.GetSteepness(normalizedX, normalizedY);

//                    // This part instantiate the building depending on whether it is within the steepness and terrain height limit
//                    if (buildingNumber > 0 && steepness <= steepnessLimit && y <= terrainHeightLimit)
//                    {
//                        GameObject clone = Instantiate(buildings[buildingNumber].building, position, Quaternion.identity);
//                        //clone.transform.localScale = buildings[buildingNumber].size;
//                    }
//                    else if (buildingNumber == -1 && steepness <= roadsteepnessLimit && y <= roadHeightLimit)
//                    {
//                        Vector3 terrainNormal = Terrain.activeTerrain.terrainData.GetInterpolatedNormal(normalizedX, normalizedY);
//                        GameObject clone = Instantiate(roads, position, Quaternion.identity);
//                        clone.transform.up = terrainNormal;
//                    }

//                    //if (roads )
//                }
//            }
//        }



//        private void MainRoad()
//        {
//            int gridMapX = (int)(habitableMapArea.x / spacing);
//            int gridMapZ = (int)(habitableMapArea.y / spacing);

//            // Change this into random direction later
//            //int length = (UnityEngine.Random.Range(gridMapZ / 2, gridMapZ)) * spacing;
//            int length = gridMapZ * spacing;
//            //Debug.Log("Length: " + length);

//            //Debug.Log("Transform Position: " + transform.position);


//            Vector3 mainStartNode = (transform.position + cityCentre + new Vector3(0, 0.01f, 0));
//            Vector3 mainEndNode = (mainStartNode + transform.forward * length);

//            //Debug.Log("Main End node: " + mainEndNode);

//            // This instantiate the main road and use the transformation rotation of the parent city generator
//            GameObject clone = Instantiate(roads, mainStartNode, transform.rotation);
//            clone.transform.localScale = Vector3.Scale(clone.transform.localScale, new Vector3(1, 1, length));

//            // Put this into road layer
//            clone.layer = 8;

//            // This makes sure that it translate the whole object by half the length you transform it
//            clone.transform.position = mainStartNode + new Vector3(0, 0, length / 2);


//            numberOfRoads = 1;

//            // Create roads that are children to this road
//            //CreateRoad(mainStartNode, mainEndNode, transform.rotation, clone);

//            // Add Road component to the main road
//            clone.AddComponent<Road>();
//            //roadsList.Add(clone);

//            int newNumberOfNodes = (int)((mainEndNode - mainStartNode).magnitude / (spacing * 3));
//            int depth = 0;

//            int numBranches = (int)Math.Ceiling(newNumberOfNodes * 2 * density);
//            for (int mainBranch = 0; mainBranch < numBranches; mainBranch++)
//            {
//                int nodeNumber = UnityEngine.Random.Range(1, newNumberOfNodes);

//                CreateRoad(mainStartNode, mainEndNode, transform.rotation, clone, nodeNumber, newNumberOfNodes, depth);
//            }
//        }

//        //// Dr Mikes analytics
//        //public int[] roadBins = new int[20];


//        private void CreateRoad(Vector3 startNode, Vector3 endNode, Quaternion rotation, GameObject previousRoad, int nodeNumber, int numberOfNodes, int depth)
//        {
//            depth++;
//            //roadBins[Mathf.Clamp(numberOfNodes, 0, 19)]++;

//            //Vector3 marker = previousRoad.transform.forward;
//            //marker = Quaternion.LookRotation(previousRoad.transform.forward, previousRoad.transform.up) * marker;

//            // Instantiate the road
//            m_marker.transform.position = startNode;
//            m_marker.transform.rotation = rotation;

//            int left = 0;
//            int right = 0;

//            // Randomly choose which direction to turn
//            int randomNumber = UnityEngine.Random.Range(0, 2) % 2;
//            if (randomNumber ==1)
//            {

//                m_marker.transform.Rotate(0, 90, 0);
//                left = 1;
//            }
//            else
//            {
//                m_marker.transform.Rotate(0, -90, 0);
//                right = 2;
//            }

//            // Sets the layer to the road layer
//            // This is set to 8, need to change it so that it can be set by the player


//            // Subdivides the road into small lengths specified as the spacing of the building


//            // Node number is the node where the new road is going to spawn
//            // This needs to be set to random later and find a way to reset nodes to start if while loop goes over
//            //int nodeNumber = 1/* UnityEngine.Random.Range(1, numberOfNodes)*/;
//            //Debug.Log("Number of Units: " + numberOfUnits);



//            // Check if another road is on this position
//            Dictionary<int, int> reference = previousRoad.GetComponent<Road>().occupiedNode;
//            bool firstIteration = true;
//            while (reference.ContainsKey(nodeNumber) && nodeNumber < numberOfNodes && firstIteration != false)
//            {
//                int roadDirection;
//                reference.TryGetValue(nodeNumber, out roadDirection);
//                int checkLeft = roadDirection & left;
//                int checkRight = roadDirection & right;
//                if ((checkLeft != 0 || checkRight != 0))
//                    nodeNumber++;
//                else
//                    break;
//                if (nodeNumber + 1 > numberOfNodes && firstIteration == true)
//                {
//                    nodeNumber = 1;
//                    firstIteration = false;
//                }
//            }
//            if (nodeNumber > numberOfNodes)
//            {
//                //Destroy(clone);
//                //destroyedRoad++;
//                return;
//            }
//            //if (!reference.ContainsKey(nodeNumber))
//            //{
//            //    int addDirection = left | right;
//            //    reference.Add(nodeNumber, addDirection);
//            //}
//            int addDirection = left | right/* | previousValue*/;
//            if (reference.ContainsKey(nodeNumber))
//            {
//                reference[nodeNumber] = reference[nodeNumber] | addDirection;
//                //int previousValue;
//                //reference.TryGetValue(nodeNumber, out previousValue);
//                //reference.Remove(nodeNumber);
//            }
//            else
//                reference.Add(nodeNumber, addDirection);
//            // Translate the position of the new road along the length of the previous road at set intervals
//            m_marker.transform.position = startNode + (previousRoad.transform.forward * nodeNumber * 3 * spacing);

//            //foreach(var element in reference)
//            //{
//            //    if (element.Key = 
//            //}

//            // Create a reference to the start node of the new road
//            Vector3 newStartNode = m_marker.transform.position;

//            // Creates a random length between 8 units and 15 units in length
//            float intendedLength = UnityEngine.Random.Range(8, maximumRoadLength) * spacing;

//            //RaycastHit hit;
//            //Physics.SphereCast(clone.transform.position, roadWidth + 2 * spacing, clone.transform.forward, out hit, intendedLength, 8);


//            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            RaycastHit[] hitColliders = Physics.SphereCastAll(m_marker.transform.position, roadWidth + spacing, m_marker.transform.forward, intendedLength, 1 << 8);
//            float roadLength = intendedLength;
//            for (int i = 0; i < hitColliders.Length; i++)
//            {
//                //if (hitColliders[i].collider.gameObject != previousRoad)
//                //    if (roadLength > hitColliders[i].distance)
//                //        roadLength = hitColliders[i].distance;

//                //if (m_marker.transform != hitColliders[i].collider.transform)
//                //{
//                float dotProduct = Vector3.Dot(hitColliders[i].collider.transform.forward, m_marker.transform.forward);

//                // This is used to check if its detecting road on the opposite side of the previous road
//                bool oppositeRoad = (previousRoad == hitColliders[i].collider.GetComponent<Road>().parentRoad);
//                if ((dotProduct > 0.99f || dotProduct < -0.99f) && !oppositeRoad)
//                /*hitColliders[i].collider.gameObject != previousRoad )*/
//                {
//                    //Destroy(clone);
//                    //return;
//                    if (roadLength > hitColliders[i].distance)
//                        roadLength = hitColliders[i].distance/* + roadWidth + spacing*//*.collider.GetComponent<Renderer>().bounds.size.z*/;

//                }
//                //}
//            }
//            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            if (roadLength < 1)
//            {
//                //Destroy(clone);
//                //destroyedRoad++;
//                return;
//            }


//            GameObject clone = Instantiate(roads, m_marker.transform.position, m_marker.transform.rotation);
//            instantiatedRoad++;

//            clone.layer = 8;

//            clone.AddComponent<Road>();

//            clone.GetComponent<Road>().parentRoad = previousRoad;


//            // Scale the road in relation to the length specified
//            //clone.transform.localScale = Vector3.Scale(clone.transform.localScale, new Vector3(1, 1, intendedLength));
//            clone.transform.localScale += new Vector3(0, 0, roadLength - 1);

//            // Translate the road to half its length so that its end connects with the previous road
//            clone.transform.Translate(new Vector3(0, 0, roadLength / 2));

//            // Create a reference to the end of the road
//            Vector3 newEndNode = clone.transform.position + clone.transform.forward * roadLength / 2;
//            //Debug.Log("End Node: " + newEndNode);

//            //roadsList.Add(clone);



//            int newNumberOfNodes = (int)((newEndNode - newStartNode).magnitude / (spacing * 3));
//            int numBranches = (int)Math.Ceiling(newNumberOfNodes * 2 * density);
//            numberOfRoads++;
//            for (int i = 0; i < numBranches; i++)
//            {
//                nodeNumber = UnityEngine.Random.Range(1, newNumberOfNodes);
//                if (numberOfRoads < maximumNoOfRoads && depth < maximumDepth)
//                    CreateRoad(newStartNode, newEndNode, clone.transform.rotation, clone, nodeNumber, newNumberOfNodes, depth);
//            }

//            //for (int branches = 0; branches < ; branches++)
//            return;
//        }

//        //private void AllocateSpace()
//        //{
//        //    foreach(GameObject roadObject in roadsList)
//        //    {
//        //        //roadObject.
//        //    }
//        //}

//        //private void AddBuildings()
//        //{

//        //}

//        private void NodeToNode()
//        {

//        }

//        private void RoadSections()
//        {

//        }

//        private void GridRandomScatter()
//        {



//        }

//        private void BuildingSize(List<Building> Buildings)
//        {

//        }
//        //private void arrangeBySize(int[,] buildingNumberArray)
//        //{
//        //    Array.Sort(buildingNumberArray, (a, b) => a.size < b.size);
//        //}

//        // Update is called once per frame
//        void Update()
//        {
//            //Vector2 positionInTerrain = new Vector2 (transform.position.normalized.x, transform.position.normalized.z);
//            //float steepness = landMap.terrainData.GetSteepness(positionInTerrain.x, positionInTerrain.y);
//            //Debug.Log("Steepness: " + steepness);
//        }
//    }
//    public class Road : MonoBehaviour
//    {
//        [HideInInspector] public Dictionary<int, int> occupiedNode = new Dictionary<int, int>();
//        [HideInInspector] public GameObject parentRoad;
//    }
//}




























////// Create a road layer
////clone.layer = 8/*LayerMask.NameToLayer("Roads")*/;
////            //Vector3 newNodeTranslation = (endNode - startNode) * (UnityEngine.Random.Range(1, unitLength))/ unitLength;
////            clone.transform.position = startNode + clone.transform.forward* spacing * UnityEngine.Random.Range(1, numberOfNodes);
//////clone.transform.Translate((endNode - startNode).normalized * UnityEngine.Random.Range(5,100) * spacing);
////newStartNode = clone.transform.position;
////            Debug.Log("MiddleNode: " + (endNode - startNode));






////            intendedLength = /*(UnityEngine.Random.Range(8, 30)) * spacing*/spacing;
////            float actualLength = spacing;
////bool parallelRoad = false;
//////RaycastHit[] hitColliders = Physics.SphereCastAll(clone.transform.position, 10 + 2 * spacing, clone.transform.forward * intendedLength, 8);
//////for (int i = 0; i < hitColliders.Length; i ++)
//////{
//////    float dotProduct = Vector3.Dot(hitColliders[i].collider.transform.forward, clone.transform.forward);
//////    if (dotProduct == 1 || dotProduct == -1)
//////    {
//////        float roadLength = hitColliders[i].collider.GetComponent<Renderer>().bounds.size.z;

//////    }
//////}
//////while (actualLength < intendedLength && parallelRoad == false)
//////{
//////    Collider[] hitColliders = Physics.OverlapSphere(clone.transform.position + clone.transform.forward * actualLength, 50000 + 2 * spacing, 8);
//////    Debug.DrawLine(clone.transform.position + clone.transform.forward * actualLength, clone.transform.position + clone.transform.forward * (actualLength + spacing));
//////    for (int i = 0; i < hitColliders.Length; i++)
//////    {
//////        float dotProduct = Vector3.Dot(hitColliders[i].transform.forward, clone.transform.forward);
//////        if (dotProduct == 1 || dotProduct == -1)
//////            parallelRoad = true;
//////    }
//////    if (parallelRoad == false)
//////        actualLength += spacing;
//////}

////parallelRoad = false;
////            clone.transform.localScale = new Vector3(1, 1, actualLength);



//////clone.transform.rotation;
////clone.transform.Translate(new Vector3(0, 0, actualLength));
////            newEndNode = (newStartNode + clone.transform.forward* actualLength);


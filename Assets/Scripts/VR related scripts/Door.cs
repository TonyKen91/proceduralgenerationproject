﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HeldObject))]
[RequireComponent(typeof(HingeJoint))]
public class Door : MonoBehaviour {

    public Transform parent;

    public float minRot;
    public float maxRot;



	// Use this for initialization
	void Start () {
        JointLimits limits = new JointLimits();
        limits.min = minRot;
        limits.max = maxRot;
        GetComponent<HingeJoint>().limits = limits;
        GetComponent<HingeJoint>().useLimits = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (parent != null)
        {
            Vector3 targetDelta = parent.position - transform.position;
            targetDelta.y = 0;

            float AngleDiff = Vector3.Angle(transform.forward, targetDelta);

            // Figures out in which direction to rotate
            Vector3 cross = Vector3.Cross(transform.forward, targetDelta);

            GetComponent<Rigidbody>().angularVelocity = cross * AngleDiff * 50f;
        }
	}

    public void PickUp()
    {
        parent = GetComponent<HeldObject>().parent.transform;
        Debug.Log("Door pick up called");
    }

    public void Drop()
    {
        parent = null;
        Debug.Log("Door drop called");
    }
}

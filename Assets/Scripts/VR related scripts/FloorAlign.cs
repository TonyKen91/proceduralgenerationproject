﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorAlign : MonoBehaviour {

    public LayerMask floorLayers;
    public Transform head;

    public float distThreshold;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 position = transform.position;
        RaycastHit hit;

        Ray ray = new Ray(head.position, Vector3.down);

        if (Physics.Raycast(ray, out hit, float.PositiveInfinity, floorLayers))
        {
            position.y = hit.point.y;
        }

        // so you won't end jumping into the next floor
        if (Vector3.Distance(transform.position, position) < distThreshold)
        {
            transform.position = position;
        }
	}
}
